//
//  MainScene.m
//  PROJECTNAME
//
//  Created by Viktor on 10/10/13.
//  Copyright (c) 2013 Apportable. All rights reserved.
//

#import "MainScene.h"

static const CGFloat scrollSpeed = 80.f;

@implementation MainScene {
    CCSprite *_hero;
    CCPhysicsNode *_physicsNode;
    CCNode *_ground1;
    CCNode *_ground2;
    NSArray *_grounds;
    CCNode *_cloud1;
    CCNode *_cloud2;
    NSArray *_clouds;
}

- (void)didLoadFromCCB {
    _grounds = @[_ground1, _ground2];
    _clouds = @[_cloud1, _cloud2];
    self.userInteractionEnabled = TRUE;
}

- (void)touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    [_hero.physicsBody applyImpulse:ccp(0, 400.f)];
}

- (void)update:(CCTime)delta {
    _hero.position = ccp(_hero.position.x + (delta * scrollSpeed), _hero.position.y);
    _physicsNode.position = ccp(_physicsNode.position.x - (delta * scrollSpeed), _physicsNode.position.y);
    for (CCNode *ground in _grounds) {
        CGPoint groundWorldPosition = [_physicsNode convertToWorldSpace:ground.position];
        CGPoint groundScreenPosition = [self convertToNodeSpace:groundWorldPosition];
        if (groundScreenPosition.x <= (-1 * ground.contentSize.width)) {
            ground.position = ccp(ground.position.x + 2 * ground.contentSize.width, ground.position.y);
        }
    }
    for (CCNode *cloud in _clouds){
        CGPoint cloudWorldPosition = [_physicsNode convertToWorldSpace:cloud.position];
        CGPoint cloudScreenPosition = [self convertToNodeSpace:cloudWorldPosition];
        if(cloudScreenPosition.x <= (-1 * cloud.contentSize.width)){
            cloud.position = ccp(cloud.position.x + 2*cloud.contentSize.width, cloud.position.y);
        }
    }
}

@end
